# Copyleft (c) December, 2021, Oromion.
# Usage: docker build -t dune-archiso/dune-archiso:dune-core .

FROM registry.gitlab.com/dune-archiso/images/dune-archiso

LABEL maintainer="Oromion <caznaranl@uni.pe>" \
  name="Dune pikaur" \
  description="Dune in pikaur" \
  url="https://gitlab.com/dune-archiso/images/dune-archiso-pikaur/container_registry" \
  vcs-url="https://gitlab.com/dune-archiso/images/dune-archiso-pikaur" \
  vendor="Oromion Aznarán" \
  version="1.0"

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ARG AUR_PIKAUR="https://aur.archlinux.org/cgit/aur.git/snapshot/pikaur.tar.gz"

RUN sudo pacman --needed --noconfirm --noprogressbar -Syyuq && \
  mkdir -p ~/build && \
  cd ~/build && \
  curl -LO ${AUR_PIKAUR} && \
  tar -xvf pikaur.tar.gz && \
  cd pikaur && \
  makepkg --noconfirm -si && \
  rm -rf ~/build ~/.cache /tmp/makepkg && \
  pacman -Qtdq | xargs -r sudo pacman --noconfirm -Rcns && \
  sudo pacman -Scc <<< Y <<< Y && \
  sudo rm -r /var/lib/pacman/sync/*